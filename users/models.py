import graphene
from django.db import models
from datetime import date
from django.utils import timezone
from django_mysql.models import ListCharField
from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.contrib.auth.models import AbstractUser

class roles(models.Model):
    role = models.CharField(max_length=220,unique=True,null=False)
    description = models.CharField(max_length=300,default="NA",null=True)

    def __str__(self):
        return str(self.role)

class Employee(AbstractUser):
    email = models.EmailField(blank=False, max_length=254, verbose_name="email address")
    EMAIL_FIELD = "email"
    creation_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)
    username = models.EmailField(blank=False, max_length=254, unique=True)
    nitor_employee_id = models.CharField( max_length=254,unique= True,blank = False)
    manager_name = models.CharField(max_length=100)
    manager_email= models.EmailField(null=True,default=None,max_length=254, verbose_name="Reporting email address")
    role = models.ForeignKey(roles, on_delete=models.SET_DEFAULT, null=True,default=None)
    isAdmin = models.BooleanField(default=False)
    country_code = models.CharField(max_length=120,default="IND")

    def __str__(self):
        return str(self.username)

class LeaveApprovalStatus(models.Model):
    status = models.CharField(max_length=120,unique= True)

    def __str__(self):
        return str(self.status)


class DefalutLeaveCountDetails(models.Model):
    general_leave_count = models.FloatField(default= 0)
    privilege_leave_count = models.FloatField(default= 0)
    optional_leave_count = models.FloatField(default= 0)
    paternity_leave_count = models.FloatField(default= 0)
    unpaid_leave_count = models.FloatField(default= 0)
    country_code = models.CharField(max_length=100)

    def __str__(self):
        return str(self.country_code)

class LeaveTypesDetails(models.Model):
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
    empolyee_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    year = models.IntegerField(default= date.today().year)

    total_general_leaves = models.FloatField(default=0)
    remaining_general_leaves = models.FloatField(default=0)

    total_privilege_leaves = models.FloatField(default=0)
    remaining_privilege_leaves = models.FloatField(default=0)

    total_optional_leaves = models.FloatField(default=0)
    remaining_optional_leaves = models.FloatField(default=0)

    total_paternity_leaves = models.FloatField(default=0)
    remaining_paternity_leaves = models.FloatField(default=0)

    total_unpaid_leaves = models.FloatField(default=0)
    remaining_unpaid_leaves = models.FloatField(default=0)


    def __str__(self):
        return str(self.empolyee_id)




#verfiy employee id with regards 2 logs
class LogDetails(models.Model):
    created_at = models.DateTimeField(default=timezone.now)
    requested_by = models.ForeignKey(Employee, on_delete=models.CASCADE,related_name="LogDetails")
    action_taken_on = models.DateTimeField(default= timezone.now)
    start_date = models.DateField(null = False)
    end_date = models.DateField(null = False)
    total_days_count = models.FloatField(null=False)
    approval_status = models.CharField( max_length=120,default="pending")
    leave_type = models.CharField(max_length=200)
    leave_note = models.CharField(max_length=200)
    notify = ArrayField(base_field=models.EmailField(null=True,default=None,max_length=254),null=True,default=None)
    actioned_by = models.ForeignKey(Employee, on_delete=models.SET_DEFAULT,default=None,null =True)
    rejected_reason = models.CharField(max_length=400, null=True, default=None)



    def __str__(self):
        return str(self.requested_by.username)

class EmailLogs(models.Model):
    email_date = models.DateTimeField(default=timezone.now)
    Log_details_id = models.ForeignKey(LogDetails, on_delete=models.CASCADE)
    to_mail = models.EmailField(blank=False, max_length=254)


class Payload(models.Model):
    success = models.BooleanField(default=False)
    msg = models.CharField(default="NA",max_length=220)
    token= models.CharField(default='NA',max_length=220)
    count = models.IntegerField(default=0)

    def __str__(self):
        return str(self.msg)