from django.core.exceptions import ValidationError
from .models  import Employee,Payload,LogDetails,LeaveApprovalStatus
import re


regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'



def validate_nitor_mail(value):
    if (re.fullmatch(regex, value)):
        if "@nitorinfotech.com" in value:
            return value
        else:
            raise ValidationError("This field accepts mail id of nitorinfotech only")
    else:
        raise ValidationError("please provide valid mail")

def validate_nitor_empid(value):
    if re.search('^NIPL[0-9]+$',value) or re.search('^NIPL[0-9]+$',value) :
        return value
    else:
        raise ValidationError("Please provide correct employee id.")


def validate_approval_status(status):
    try:
        count = LeaveApprovalStatus.objects.filter(status = status).count()
        if count == 1:
            return status
        else :
            raise ValidationError("Please provide correct approval status.")

    except Exception as e:
        raise ValidationError(e)

def failed_op(msg,cnt=0):
    count = Payload.objects.filter(pk=1).count()
    if count == 0 :
        create_paylod = Payload.objects.create(msg=msg, success=False,count=cnt)
        return create_paylod
    else:
        payload = Payload.objects.get(pk=1)
        payload.msg = msg
        payload.success = False
        payload.count=cnt
        return payload


def success_op(msg,cnt=0):
    count = Payload.objects.filter(pk=1).count()
    if count == 0 :
        create_paylod = Payload.objects.create(msg=msg,success=True,count=cnt)
        return create_paylod
    else :
        payload = Payload.objects.get(pk=1)
        payload.msg = msg
        payload.success = True
        payload.count = cnt
    return payload


def check_authorization_of_user(username):
    try:
        count = Employee.objects.filter(username=username,isAdmin=True).count()
        if count == 1 :
            return True
        else:
            return False
    except Exception as E:
        print(E)
        raise ValidationError(E)
