from django.contrib import admin
from .models  import roles, Employee,LogDetails,LeaveTypesDetails,DefalutLeaveCountDetails

admin.site.register(roles)
admin.site.register(Employee)
admin.site.register(LogDetails)
admin.site.register(LeaveTypesDetails)
admin.site.register(DefalutLeaveCountDetails)
#admin.site.register(LeaveDetails)
