import graphene
from datetime import date,datetime
from django.utils import timezone
import pytz
from django.core.exceptions import ValidationError
from .views import leave_approval,leave_notify
from .validations import validate_nitor_mail,validate_nitor_empid,validate_approval_status
from .types import EmployeeType,PayloadType,LogDetailsType,LeaveTypesDetailsType,DefalutLeaveCountDetailsType,LeaveApprovalStatusType
from .models import Employee,LogDetails,LeaveTypesDetails,DefalutLeaveCountDetails,LeaveApprovalStatus
from .validations import validate_nitor_mail,success_op,failed_op,check_authorization_of_user


class addLeaveApprovalStatus(graphene.Mutation):
    class Arguments:
        status = graphene.String(required = True )

    payload = graphene.Field(PayloadType)
    leave_status = graphene.Field(LeaveApprovalStatusType)

    @classmethod
    def mutate(cls,root,info,status = None):
        try:
            obj = LeaveApprovalStatus.objects.create(status = status)
            msg  = "Status has stored successfully."
            return addLeaveApprovalStatus(leave_status = obj,payload = success_op(msg=msg))
        except Exception as e:
            return addLeaveApprovalStatus(payload = failed_op(msg=e))


def add_leave_count_to_back(log_obj):
    obj = LeaveTypesDetails.objects.get(empolyee_id=log_obj.requested_by,
                                        year=date.today().year)
    if log_obj.leave_type == "general":
        obj.remaining_general_leaves = obj.remaining_general_leaves + log_obj.total_days_count
        obj.updated_at = timezone.now()
        obj.save()

    if log_obj.leave_type == "privilege":
        obj.remaining_privilege_leaves = obj.remaining_privilege_leaves + log_obj.total_days_count
        obj.updated_at = timezone.now()
        obj.save()

    if log_obj.leave_type == "optional":
        obj.remaining_optional_leaves = obj.remaining_optional_leaves + log_obj.total_days_count
        obj.updated_at = timezone.now()
        obj.save()

    if log_obj.leave_type == "paternity":
        obj.remaining_paternity_leaves = obj.remaining_paternity_leaves + log_obj.total_days_count
        obj.updated_at = timezone.now()
        obj.save()

    if log_obj.leave_type == "unpaid":
        obj.remaining_unpaid_leaves = obj.remaining_unpaid_leaves + log_obj.total_days_count
        obj.updated_at = timezone.now()
        obj.save()


def deduct_from_remaining_leave(log_obj):
    obj = LeaveTypesDetails.objects.get(empolyee_id=log_obj.requested_by,
                                        year=date.today().year)
    if log_obj.leave_type == "general":
        if obj.remaining_general_leaves >= log_obj.total_days_count:
            obj.remaining_general_leaves = obj.remaining_general_leaves - log_obj.total_days_count
            obj.updated_at = timezone.now()
            obj.save()
        else:
            log_obj.delete()
            raise ValidationError("Remaining leave count is less than applied count.")

    if log_obj.leave_type == "privilege":
        if obj.remaining_privilege_leaves >= log_obj.total_days_count:
            obj.remaining_privilege_leaves = obj.remaining_privilege_leaves - log_obj.total_days_count
            obj.updated_at = timezone.now()
            obj.save()
        else:
            log_obj.delete()
            raise ValidationError("Remaining leave count is less than applied count.")

    if log_obj.leave_type == "optional":
        if obj.remaining_optional_leaves >= log_obj.total_days_count:
            obj.remaining_optional_leaves = obj.remaining_optional_leaves - log_obj.total_days_count
            obj.updated_at = timezone.now()
            obj.save()
        else:
            log_obj.delete()
            raise ValidationError("Remaining leave count is less than applied count.")

    if log_obj.leave_type == "paternity":
        if obj.remaining_paternity_leaves >= log_obj.total_days_count:
            obj.remaining_paternity_leaves = obj.remaining_paternity_leaves - log_obj.total_days_count
            obj.updated_at = timezone.now()
            obj.save()
        else:
            log_obj.delete()
            raise ValidationError("Remaining leave count is less than applied count.")

    if log_obj.leave_type == "unpaid":
        if obj.remaining_unpaid_leaves >= log_obj.total_days_count:
            obj.remaining_unpaid_leaves = obj.remaining_unpaid_leaves - log_obj.total_days_count
            obj.updated_at = timezone.now()
            obj.save()
        else:
            log_obj.delete()
            raise ValidationError("Remaining leave count is less than applied count.")


class getManagerLeaveApproval(graphene.Mutation):
    class Arguments:
        logDetailsId = graphene.Int(required = True)
        status = graphene.String(required = True)
        rejected_reason = graphene.String()

    payload =graphene.Field(PayloadType)
    leave = graphene.Field(LogDetailsType)

    @classmethod
    def mutate(cls,root,info, logDetailsId = None,status = None, rejected_reason = None):
        try :
            exist_count = LogDetails.objects.filter(id = logDetailsId).count()
            if exist_count == 1:
                obj = LogDetails.objects.get(id=logDetailsId)
                if obj.approval_status == "pending":
                    if status == "rejected" or  status == "cancelled":
                        add_leave_count_to_back(obj)
                        obj.actioned_by = obj.actioned_by
                        obj.save()

                if obj.approval_status == "rejected" or  obj.approval_status == "cancelled":
                    if status == "approved":
                        deduct_from_remaining_leave(obj)

                status = validate_approval_status(status)
                obj.approval_status = status
                obj.action_taken_on = timezone.now()
                obj.rejected_reason = rejected_reason
                obj.save()

                msg = "Leave approval status updated successfully."
                return getManagerLeaveApproval(leave = obj, payload=success_op(msg=msg))

            else :
                msg = "Please provide correct log detail id."
                return getManagerLeaveApproval(payload= failed_op(msg=msg))

        except Exception as e:
            return getManagerLeaveApproval(payload = failed_op(msg = e))

class addDefalutLeaveCountDetails(graphene.Mutation):
    class Arguments:
        general_leave_count = graphene.Float(required = True)
        privilege_leave_count = graphene.Float(required = True)
        optional_leave_count = graphene.Float(required = True)
        paternity_leave_count = graphene.Float(required=True)
        unpaid_leave_count = graphene.Float(required=True)
        country_code = graphene.String(required = True)

    payload = graphene.Field(PayloadType)
    default_leave = graphene.Field(DefalutLeaveCountDetailsType)

    @classmethod
    def mutate(cls,root,info,general_leave_count=None,
               privilege_leave_count=None,
               optional_leave_count = None,
               paternity_leave_count = None,
               unpaid_leave_count = None,
               country_code=None):
        try:
            exist_count = DefalutLeaveCountDetails.objects.filter(country_code = country_code).count()

            if exist_count == 1:
                leave_obj = DefalutLeaveCountDetails.objects.get(country_code = country_code)
                leave_obj.general_leave_count = general_leave_count
                leave_obj.privilege_leave_count = privilege_leave_count
                leave_obj.optional_leave_count = optional_leave_count
                leave_obj.paternity_leave_count = paternity_leave_count
                leave_obj.unpaid_leave_count = unpaid_leave_count
                leave_obj.save()
            else:
                leave_obj = DefalutLeaveCountDetails.objects.create(general_leave_count = general_leave_count,
                                                                    privilege_leave_count = privilege_leave_count,
                                                                    optional_leave_count = optional_leave_count,
                                                                    paternity_leave_count = paternity_leave_count,
                                                                    unpaid_leave_count = unpaid_leave_count,
                                                                    country_code = country_code)

            if leave_obj:
                msg = "Default leave count details has stored successfully."
                return addDefalutLeaveCountDetails(default_leave = leave_obj,payload = success_op(msg=msg))

        except Exception as e:
            return addDefalutLeaveCountDetails(payload = failed_op(msg=e))

class addLogDetail(graphene.Mutation):
    class Arguments:
        requested_by =graphene.String(required=True)
        startDate = graphene.String(required=True)
        endDate = graphene.String(required=True)
        totalDaysCount = graphene.Float(required=True)
        leaveType = graphene.String(required=True)
        leaveNote = graphene.String(required=True)
        notify = graphene.List(graphene.String)

    payload = graphene.Field(PayloadType)
    log = graphene.Field(LogDetailsType)

    @classmethod
    def mutate(cls,root,info,requested_by=None,
               startDate=None,
               endDate=None,
               totalDaysCount=None,
               leaveType=None,
               leaveNote=None,
               notify=None):
        try:

            requested_by = validate_nitor_mail(requested_by)
            requested_by_obj = Employee.objects.get(username = requested_by)
            start_date_obj = datetime.strptime(str(startDate), '%Y-%m-%dT%H:%M:%S.%fZ').date()
            end_date_obj = datetime.strptime(str(endDate), '%Y-%m-%dT%H:%M:%S.%fZ').date()


            log_obj = LogDetails.objects.create(requested_by= requested_by_obj,
                                            start_date = start_date_obj,
                                            end_date = end_date_obj,
                                            total_days_count = totalDaysCount,
                                            approval_status = "pending",
                                            leave_type = leaveType,
                                            leave_note = leaveNote
                                            )
            if log_obj is not None:
                deduct_from_remaining_leave(log_obj)
                if notify :
                    log_obj.notify = notify
                    log_obj.save()
                    leave_notify(log_obj)

                leave_approval(requested_by,requested_by_obj.manager_email)

            msg = "Leave log stored successfully."
            return addLogDetail(log=log_obj, payload=success_op(msg))

        except Exception as e :
            print(e)
            return  addLogDetail(payload = failed_op(msg=e))


class addLeaveTypeDetails(graphene.Mutation):
    class Arguments:
        employeeEmail = graphene.String(required = True)
        totalGenaralLeaves = graphene.Float()
        totalPrivilegeLeaves = graphene.Float()
        totalOptionaLeaves = graphene.Float()
        totalUnpaidLeaves = graphene.Float()
        totalPaternityLeaves = graphene.Float()
        remainingUnpaidLeaves = graphene.Float()
        remainingPaternityLeaves = graphene.Float()
        remainingOptionalLeaves = graphene.Float()
        remainingGeneralLeaves = graphene.Float()
        remainingPrivilegeLeaves = graphene.Float()
        year = graphene.Int(required = True)

    payload = graphene.Field(PayloadType)
    leaveType = graphene.Field(LeaveTypesDetailsType)

    @classmethod
    def mutate(cls,root,info,
               employeeEmail = None,
               totalOptionaLeaves = None,
               totalUnpaidLeaves = None,
               totalPaternityLeaves = None,
               totalGenaralLeaves = None,
               totalPrivilegeLeaves = None,
               remainingGeneralLeaves = None,
               remainingPrivilegeLeaves = None,
               remainingUnpaidLeaves = None,
               remainingPaternityLeaves = None,
               remainingOptionalLeaves = None,
               year = None):
        try:
            exist_emp_count = Employee.objects.filter(username=employeeEmail).count()

            if exist_emp_count == 1:
                user_obj = Employee.objects.get(username=employeeEmail)
                default_leave_obj = DefalutLeaveCountDetails.objects.get(country_code = user_obj.country_code)
            else :
                msg = "Please provide registered users details."
                return addLeaveTypeDetails( payload=failed_op(msg))

            exist_count = LeaveTypesDetails.objects.filter(empolyee_id = user_obj,year=year).count()
            if exist_count == 1 :
                exist_leave_obj = LeaveTypesDetails.objects.get(empolyee_id=user_obj, year=year)

                exist_leave_obj.remaining_general_leaves = remainingGeneralLeaves
                exist_leave_obj.remaining_privilege_leaves = remainingPrivilegeLeaves
                exist_leave_obj.remaining_optional_leaves = remainingOptionalLeaves
                exist_leave_obj.remaining_paternity_leaves = remainingPaternityLeaves
                exist_leave_obj.remaining_unpaid_leaves = remainingUnpaidLeaves
                exist_leave_obj.total_general_leaves = totalGenaralLeaves
                exist_leave_obj.total_privilege_leaves = totalPrivilegeLeaves
                exist_leave_obj.total_optional_leaves = totalOptionaLeaves
                exist_leave_obj.total_paternity_leaves = totalPaternityLeaves
                exist_leave_obj.total_unpaid_leaves = totalUnpaidLeaves

                exist_leave_obj.save()

                msg = "Leave types details has stored successfully."
                return addLeaveTypeDetails(leaveType=exist_leave_obj, payload=success_op(msg))
            else:
                leave_type_obj = LeaveTypesDetails.objects.create(empolyee_id = user_obj,
                                                                  remaining_general_leaves = default_leave_obj.general_leave_count ,
                                                                  remaining_privilege_leaves = default_leave_obj.privilege_leave_count,
                                                                  remaining_optional_leaves = default_leave_obj.optional_leave_count,
                                                                  remaining_paternity_leaves = default_leave_obj.paternity_leave_count,
                                                                  remaining_unpaid_leaves = default_leave_obj.unpaid_leave_count,
                                                                  total_general_leaves = default_leave_obj.general_leave_count,
                                                                  total_privilege_leaves = default_leave_obj.privilege_leave_count,
                                                                  total_optional_leaves = default_leave_obj.optional_leave_count,
                                                                  total_paternity_leaves = default_leave_obj.paternity_leave_count,
                                                                  total_unpaid_leaves = default_leave_obj.unpaid_leave_count,
                                                                  year = year)

                msg = "Leave types details has stored successfully."
                return addLeaveTypeDetails(leaveType =leave_type_obj, payload=success_op(msg))

        except Exception as e:
            return addLeaveTypeDetails(payload = failed_op(msg= e))

class RegisterUser(graphene.Mutation):
    class Arguments :
        username = graphene.String(required=True)
        password1 = graphene.String(required=True)
        password2 = graphene.String(required=True)
        first_name = graphene.String(required = True)
        last_name = graphene.String(required = True)
        nitor_employee_id = graphene.String(required=True)
        manager_name = graphene.String(required= True)
        manager_email = graphene.String(required = True)
        country_code = graphene.String(required = True)

    user = graphene.Field(EmployeeType)
    payload = graphene.Field(PayloadType)

    @classmethod
    def mutate(cls,root,info,
               username,
               password1,
               password2,
               first_name,
               last_name,
               country_code,
               nitor_employee_id,
               manager_name,manager_email):
        try:
         is_verified = validate_nitor_mail(username)
         if is_verified:
            user = username
            email = user
            if password1 == password2:
                if len(password1) >= 8 :
                        password = password1
                        count = Employee.objects.filter(username=user).count()
                        if count == 0 :
                            manager_email = validate_nitor_mail(manager_email)
                            nitor_employee_id = validate_nitor_empid(nitor_employee_id)
                            default_leave_obj = DefalutLeaveCountDetails.objects.get(country_code = country_code)
                            new_user = Employee.objects.create_user(username=user,
                                                                    password=password,
                                                                    email=email,
                                                                    first_name = first_name,
                                                                    last_name = last_name,
                                                                    nitor_employee_id = nitor_employee_id,
                                                                    manager_name = manager_name,
                                                                    manager_email = manager_email,
                                                                    country_code = country_code)
                            new_user.is_active = True
                            new_user.save()

                            leave_type_obj = LeaveTypesDetails.objects.create(empolyee_id=new_user,
                                                                              remaining_general_leaves=default_leave_obj.general_leave_count,
                                                                              remaining_privilege_leaves=default_leave_obj.privilege_leave_count,
                                                                              remaining_optional_leaves=default_leave_obj.optional_leave_count,
                                                                              remaining_paternity_leaves=default_leave_obj.paternity_leave_count,
                                                                              remaining_unpaid_leaves=default_leave_obj.unpaid_leave_count,
                                                                              total_general_leaves=default_leave_obj.general_leave_count,
                                                                              total_privilege_leaves=default_leave_obj.privilege_leave_count,
                                                                              total_optional_leaves=default_leave_obj.optional_leave_count,
                                                                              total_paternity_leaves=default_leave_obj.paternity_leave_count,
                                                                              total_unpaid_leaves=default_leave_obj.unpaid_leave_count,
                                                                              )

                            msg = "user successfully registered and default leaves count added successfully"

                            return RegisterUser(user=new_user,payload=success_op(msg))
                        else:
                            msg = "current user has already exist."
                            return RegisterUser(payload=failed_op(msg))
                else:
                    msg = "password is very weak, please provide strong password."
                    return RegisterUser(payload=failed_op(msg))
            else :
                msg = "please provide correct password."
                return RegisterUser(payload=failed_op(msg))
         else :
             msg = "Please Provide correct mail id or Employee id."
             return RegisterUser(payload=failed_op(msg))
        except Exception as e:
            print(e)
            msg = e
            return RegisterUser(payload=failed_op(msg))


class DeleteUSer(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)

    user = graphene.Field(EmployeeType)
    payload = graphene.Field(PayloadType)

    @classmethod
    def mutate(cls,root,info,username):
        try :
            is_verified = validate_nitor_mail(username)
            if is_verified:
                login_user = info.context.user
                valid_user = check_authorization_of_user(login_user)
                if valid_user:
                    cnt = Employee.objects.filter(username=username).count()
                    if cnt == 1:
                        user = Employee.objects.get(username=username)
                        user.is_active = False
                        user.save()
                        msg = "User has updated as Inactive."
                        return DeleteUSer(user=user,payload=success_op(msg))
                    else :
                        msg = "Please provide correct username as we are not getting data related username."
                        return DeleteUSer(payload=failed_op(msg))
                else:
                    msg = "User has not authorized to perform this task"
                    return DeleteUSer(payload=failed_op(msg))
        except Exception as e:
            print(e)
            msg = e
            return DeleteUSer(payload=failed_op(msg))