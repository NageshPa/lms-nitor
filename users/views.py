from django.shortcuts import render
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.dispatch import receiver
from django.http import HttpResponse
from django.core.mail import send_mail
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage
from django.conf import settings

def leave_approval(requested_by,approved_by):
    link = ""
    ctx = {
        'pending_approval_link': link,
        'email': requested_by
    }
    message = get_template('pendingapproval.html').render(ctx)
    to = approved_by
    subject = "Nitor LMS leave - approval pending"
    msg = EmailMessage(subject, message, settings.EMAIL_HOST_USER, [to])
    msg.content_subtype = "html"
    msg.send()
    return HttpResponse("Mail successfully sent")

def leave_notify(log_obj):
    link = ""
    ctx = {
        'pending_approval_link': link,
        'email': log_obj.requested_by,
        'start_date': log_obj.start_date,
        'end_date' : log_obj.end_date
    }
    message = get_template('leaveNotification.html').render(ctx)
    to = log_obj.notify
    subject = "Nitor LMS Leave Notification"
    msg = EmailMessage(subject, message, settings.EMAIL_HOST_USER, to)
    msg.content_subtype = "html"  # Main content is now text/html
    msg.send()
    return HttpResponse("Mail successfully sent")

