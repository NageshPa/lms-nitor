import graphene
from graphql_auth import mutations
import graphql_jwt
from .queries import UsersQuery
from .Mutation import RegisterUser,DeleteUSer,addLogDetail,getManagerLeaveApproval,addLeaveApprovalStatus,addDefalutLeaveCountDetails,addLeaveTypeDetails

class Query(UsersQuery,graphene.ObjectType):
    pass


class AuthMutation(graphene.ObjectType):
   token_auth = mutations.ObtainJSONWebToken.Field()
   token_auth = mutations.ObtainJSONWebToken.Field()
   verify_token = graphql_jwt.Verify.Field()
   refresh_token = graphql_jwt.Refresh.Field()

class Mutation(AuthMutation,graphene.ObjectType):
    register_user = RegisterUser.Field()
    delete_user = DeleteUSer.Field()
    apply_leave = addLogDetail.Field()
    addLeaveTypeDetails = addLeaveTypeDetails.Field()
    addDefalutLeaveCountDetails = addDefalutLeaveCountDetails.Field()
    addLeaveApprovalStatus = addLeaveApprovalStatus.Field()
    getManagerLeaveApproval = getManagerLeaveApproval.Field()

schema = graphene.Schema(query=Query,mutation=Mutation)