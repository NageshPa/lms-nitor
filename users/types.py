import graphene
import pytz
from datetime import date,datetime
from graphene_django import DjangoObjectType
from .models import Employee, roles, LogDetails,Payload,LeaveTypesDetails,DefalutLeaveCountDetails,LeaveApprovalStatus


class EmployeeType(DjangoObjectType):
    class Meta:
        model = Employee
        fields = "__all__"

class LeaveApprovalStatusType(DjangoObjectType):
    class Meta:
        model = LeaveApprovalStatus
        fields = "__all__"

class DefalutLeaveCountDetailsType(DjangoObjectType):
    class Meta:
        model =  DefalutLeaveCountDetails
        fields = "__all__"


class LeaveTypesDetailsType(DjangoObjectType):
    class Meta:
        model = LeaveTypesDetails
        fields = "__all__"


class LogDetailsType(DjangoObjectType):
    class Meta :
        model = LogDetails
        fields = "__all__"

    utcStartDate = graphene.String()
    utcEndDate = graphene.String()

    def resolve_utcStartDate(self, info):
        local = pytz.timezone("UTC")
        naive = datetime.strptime(str(self.start_date), "%Y-%m-%d")
        local_date = local.localize(naive, is_dst=None)
        utc_date = local_date.astimezone(pytz.utc)
        return utc_date.isoformat()

    def resolve_utcEndDate(self, info):
        local = pytz.timezone("UTC")
        naive = datetime.strptime(str(self.end_date), "%Y-%m-%d")
        local_date = local.localize(naive, is_dst=None)
        utc_date = local_date.astimezone(pytz.utc)
        return utc_date.isoformat()

class PayloadType(DjangoObjectType):
    class Meta:
        model = Payload
        fields = "__all__"

class RoleType(DjangoObjectType):
    class Meta:
        model = roles
        fields = "__all__"