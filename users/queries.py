import graphene
from .validations import validate_nitor_mail,validate_nitor_empid
import graphql_jwt
from .models import Employee,LeaveApprovalStatus,DefalutLeaveCountDetails,LogDetails,LeaveTypesDetails
from .types import EmployeeType,LeaveApprovalStatusType,DefalutLeaveCountDetailsType,LogDetailsType,LeaveTypesDetailsType

class UsersQuery(graphene.ObjectType):
    default_limit,default_offset = 5,1
    data = graphene.List(EmployeeType,
                         limit= graphene.Int(),
                         offset= graphene.Int())
    leave_approval_status = graphene.List(LeaveApprovalStatusType,
                                              limit= graphene.Int(),
                                              offset= graphene.Int())

    leave_log_details = graphene.List(LogDetailsType,
                                      email = graphene.String(required = True),
                                      limit=graphene.Int(),
                                      offset=graphene.Int())
    leave_log_details_count = graphene.Int(email = graphene.String(required = True))

    leave_type_details = graphene.List(LeaveTypesDetailsType,
                                      email = graphene.String(required = True),
                                      limit=graphene.Int(),
                                      offset=graphene.Int())

    users_details = graphene.List(EmployeeType,
                                  limit=graphene.Int(),
                                  offset=graphene.Int())

    notify_user_details = graphene.List(EmployeeType)

    default_leave_count_details = graphene.List(DefalutLeaveCountDetailsType)

    def resolve_leave_log_details_count(self,info,email):
        mail = validate_nitor_mail(email)
        user_obj = Employee.objects.get(username=email)
        return LogDetails.objects.filter(requested_by=user_obj).count()

    def resolve_notify_user_details(self,info):
        return Employee.objects.all()

    def resolve_users_details(self,info,
                              limit=default_limit,
                              offset=default_offset):
        return Employee.objects.all()[limit*(offset-1):limit + (offset-1)*limit]

    def resolve_leave_type_details(self,info,email,
                                   limit=default_limit,
                                   offset=default_offset):
        email = validate_nitor_mail(email)
        user_obj = Employee.objects.get(username=email)
        return LeaveTypesDetails.objects.filter(empolyee_id=user_obj)


    def resolve_leave_log_details(self,info,email,
                                  limit=default_limit,
                                  offset=default_offset):
        email = validate_nitor_mail(email)
        user_obj = Employee.objects.get(username=email)
        return LogDetails.objects.filter(requested_by=user_obj).order_by('-action_taken_on')[limit*(offset-1):limit + (offset-1)*limit]


    def resolve_default_leave_count_details(self,info):
        return DefalutLeaveCountDetails.objects.all()


    def resolve_leave_approval_status(self,info,limit=default_limit,offset=default_offset):
        return LeaveApprovalStatus.objects.all()[limit*(offset-1):limit + (offset-1)*limit]

    def resolve_data(self,info,limit=default_limit,offset=default_offset):
        return Employee.objects.all()[limit*(offset-1):limit + (offset-1)*limit]
